var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');

require('dotenv').config();

mongoose.connect(process.env['MONGO_ATLAS'], { useNewUrlParser: true }, (err) => {
  if (err) {
    return console.error(err);
  }
  console.log('Database connection successful!');
});

var indexRouter = require('./routes/index');
var userRouter = require('./routes/user');
//var usersRouter = require('./routes/users');
var testRouter = require('./routes/test');

var app = express();

//handle CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*"); //change to domain!
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  if (req.method == "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//use index.html
app.use(express.static(path.join(__dirname, 'public')));

//routes
app.use('/', indexRouter);
app.use('/test', testRouter);
app.use('/user', userRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.render('error');
  res.status(500).json({
    message: err.message,
    error: err
  })
});

module.exports = app;
