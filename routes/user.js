var express = require('express');
var router = express.Router();
const checkAuth = require('../middlewares/check-auth')

const UserController = require('../controllers/user');

//public routes:
router.post('/signup', UserController.user_signup);
router.post('/login', UserController.user_login);

//private routes:
//user CRUD
router.get('/all', checkAuth, UserController.user_get_all);
router.get('/:userId', checkAuth, UserController.user_get_data)
router.patch('/:userId', checkAuth, UserController.user_update);
router.delete('/:userId', checkAuth, UserController.user_delete);
//trials/query
router.post('/save_trials', checkAuth, UserController.user_save_trials)
router.post('/save_query', checkAuth, UserController.user_save_query)
router.post('/delete_trial', checkAuth, UserController.user_delete_trial)
router.post('/delete_query', checkAuth, UserController.user_delete_query)

module.exports = router;