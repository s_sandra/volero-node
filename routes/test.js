var express = require('express')
var router = express.Router();

const UserController = require ('../controllers/user');

router.get('/', function(req, res, next){
    res.status(200).json({
        message: "handling get request test  aaaaaaaaaaa-> done"
    })
})

router.get('/all', UserController.user_get_all);
router.post('/signup', UserController.user_signup);

module.exports = router;