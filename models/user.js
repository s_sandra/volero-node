const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true)

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstname: {type: String, required: false},
    lastname: {type: String, required: false},
    email: {type: String, required: true, match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/},
    username: {type: String, required: true, unique: true,},
    password: {type: String, required: true},
    saved_trials: [{
            trials_group_name: String,
            trials: []
        }],
    saved_queries: [{
        query_name: String,
        query_param: String
    }]
});

module.exports = mongoose.model('User', userSchema);