const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

//User CRUD

exports.user_get_all = (req, res, next) => {
    User.find()
        /*.select('_id firstname lastname email username password')*/
        .exec()
        .then(users => {
            console.log(users);
            res.status(200).json(users);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
}

exports.user_signup = (req, res, next) => {
    User.find({ username: req.body.username })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.status(409).json({
                    message: "User exists"
                });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        })
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            firstname: req.body.firstname,
                            lastname: req.body.lastname,
                            email: req.body.email,
                            username: req.body.username,
                            password: hash
                            //saved_trials: [{trial_name: "Trial name 1", "trial_phase": "Phase 1"}, {trial_name: "Trial name 2", "trial_phase": "Phase3"}]
                        });
                        user.save(function (error, user) {
                            if (error) {
                                return res.status(500).json({
                                    message: error
                                });
                            }
                            if (user) {
                                const token = jwt.sign({
                                    email: user.email,
                                    userId: user._id
                                },
                                    process.env.JWT_KEY,
                                    {
                                        expiresIn: "1h"
                                    }
                                );
                                return res.status(200).json({
                                    message: "Auth successful",
                                    token: token,
                                    user: user
                                })
                            }
                            return res.status(500).json({
                                message: error
                            });
                        })
                    }
                });
            }
        })
}

exports.user_login = (req, res, next) => {
    User.find({ username: req.body.username })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: "Auth failed!"
                });
            }
            bcrypt.compare(req.body.password, user[0].password, (error, result) => {
                if (error) {
                    return res.status(401).json({
                        message: "Auth failed!"
                    });
                }
                if (result) {
                    const token = jwt.sign({
                        email: user[0].email,
                        userId: user[0]._id
                    },
                        process.env.JWT_KEY,
                        {
                            expiresIn: "1h"
                        }
                    );
                    return res.status(200).json({
                        message: "Auth successful",
                        token: token,
                        user: user[0]
                    })
                }
                return res.status(401).json({
                    message: "Auth failed!"
                });
            })
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                error: error
            });
        })
}

exports.user_get_data = (req, res, next) => {
    const id = req.params.userId;
    User.findById(id, function (err, user) {
        if (err) {
            console.log(err)
            return res.status(500).json({
                error: err
            })
        } else {
            return res.status(201).json({
                message: "User found!",
                user: user
            });
        }
    })
}

exports.user_update = (req, res, next) => {
    const id = req.params.userId;
    const updateObj = {};
    for (const prop in req.body) {
        if (req.body[prop] !== "") {
            if (prop === "password") {
                bcrypt.hash(req.body[prop], 10, (err, hash) => {
                    console.log("Value", req.body[prop]);
                    console.log(hash);
                    if (err) {
                        return res.status(500).json({
                            error: err
                        })
                    } else {
                        User.findOneAndUpdate({ _id: id }, { password: hash })
                            .exec()
                            .then(result => {
                            })
                            .catch(err => {
                            })
                    }
                })
            }
            updateObj[prop] = req.body[prop];
        }
    }
    User.findOneAndUpdate({ _id: id }, { $set: updateObj })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "User updated!"
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.user_delete = (req, res, next) => {
    User.remove({ _id: req.params.userId })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "User deleted!"
            });
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            });
        });
}

//trials/query methods

exports.user_save_trials = (req, res, next) => {
    const id = req.body.user_id;
    const trials_group_name = req.body.trials_group_name
    const trials = req.body.trials_ids;
    User.findById(id, function (err, user) {
        if (!err) {
            user.saved_trials.push({
                trials_group_name: trials_group_name,
                trials: trials
            });
        }
        user.save(function (err, user) {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    error: err
                })
            } else {
                return res.status(200).json({
                    message: "Trials saved!",
                    user: user
                });
            }
        });
    })
}


exports.user_save_query = (req, res, next) => {
    const id = req.body.user_id;
    const query_name = req.body.query_name
    const query_param = req.body.query_param;
    User.findById(id, function (err, user) {
        if (!err) {
            user.saved_queries.push({
                query_name: query_name,
                query_param: query_param
            });
        }
        user.save(function (err, user) {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    error: err
                })
            } else {
                return res.status(200).json({
                    message: "Query saved!",
                    user: user
                });
            }
        });
    })
}



exports.user_delete_trial = (req, res, next) => {
    const user_id = req.body.user_id
    const trial_id = req.body.trial_id
    User.findById(user_id, function (err, user) {
        if (!err) {
            user.saved_trials.pull(trial_id);
            user.save(function (err, user) {
                if (err) {
                    console.log(err)
                    return res.status(500).json({
                        error: err
                    })
                } else {
                    return res.status(200).json({
                        message: "Trial deleted!",
                        user: user
                    });
                }
            });
        } else {
            return res.status(200).json({
                message: "User not found!",
                error: err
            });
        }
    })
}

exports.user_delete_query = (req, res, next) => {
    const user_id = req.body.user_id
    const query_id = req.body.query_id
    User.findById(user_id, function (err, user) {
        if (!err) {
            user.saved_queries.pull(query_id);
            user.save(function (err, user) {
                if (err) {
                    console.log(err)
                    return res.status(500).json({
                        error: err
                    })
                } else {
                    return res.status(200).json({
                        message: "Query deleted!",
                        user: user
                    });
                }
            });
        } else {
            return res.status(200).json({
                message: "User not found!",
                error: err
            });
        }
    })
}

